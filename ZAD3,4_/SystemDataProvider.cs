﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3_4_
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;

        public SystemDataProvider() : base() {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;

        }

        public float GetCPULoad() {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad) {
                this.Notify();


            }

            this.previousCPULoad = currentLoad;
            return currentLoad;




        }
        public float GetAvailableRAM() {

            float CurrentRAM = this.AvailableRAM;
            return CurrentRAM;

        }


        //public float GetAvailableRAM()
        //{
        //    float currentram = this.AvailableRAM;
        //    if (currentram > previousRAMAvailable * 1.1 || currentram < previousRAMAvailable * 0.9)
        //    {
        //        this.Notify();
        //    }
        //    this.previousRAMAvailable = currentram;
        //    return currentram;
        //}





    }
}
