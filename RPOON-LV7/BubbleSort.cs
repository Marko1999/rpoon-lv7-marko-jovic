﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV7
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {

            int arraySize = array.Length;

            for (int i = 1; i <= array.Length - 1; ++i)

                for (int j = 0; j < array.Length - i; ++j)

                    if (array[j] > array[j + 1])


                        Swap(ref array[j], ref array[j + 1]);


        }


    }
}
